(function(){
    "use strict";

    const STEP_NAME = "concept2";
    const IMG_ID = "our_farm";
    const TITLE_ID = "green";
    const HIDE_STEP = "form";

    console.log("SHOW / HIDE inited");

    let show = function(image){
        image.style.display = 'inline-block';
        image.style.marginTop = '-80px';
        let title = document.getElementById(TITLE_ID);
        title.style.fontSize = '150px';
    };

    let hide = function(image){
        image.style.display = 'none';
        let title = document.getElementById(TITLE_ID);
        title.style.fontSize = '180px';
    };

    let check = function(){
        let current = window.location.hash;

        if(current == '#/' + STEP_NAME){
            let img = document.getElementById(IMG_ID);
            show(img);
        }

        if(current == '#/' + HIDE_STEP){
            let img = document.getElementById(IMG_ID);
            hide(img);
        }
    };

    setInterval(check, 1000);
}())
