(function(PresentMe){
    let config = {
        theme: "Green",
        progressBar: true,

        fonts: {
            h1: "Lobster",
            h2: "Lobster",
            p: "Lobster",
            li: "Lobster"
        },

        backfaceVisibility: "visible"
    }

    PresentMe.config = config;
})(PresentMe);
